#ifndef __PARSE_HPP__
#define __PARSE_HPP__

#include "asset.hpp"
#include "portfolio.hpp"
#include <cstdio>
#include <cstdlib>
#include <string>
#include <vector>
#include <Eigen/Dense>
using namespace Eigen;

void split(std::string& input, std::vector<std::string>& vec, std::string& del);
std::vector<Asset> readUni(char* file,int &num);
MatrixXd readCorr(char* file,int num);
double Unrestrict(Portfolio &P,double &r); 
double Restrict(Portfolio &P,double &r);
#endif
