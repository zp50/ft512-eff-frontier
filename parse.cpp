#include "asset.hpp"
#include "portfolio.hpp"
#include "parse.hpp"
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <vector>
#include <Eigen/Dense>
using namespace Eigen;
//using namespace std;

void split(std::string& input, std::vector<std::string>& vec, std::string& del)
{
  std::string::size_type indx1;
  std::string::size_type indx2;
  indx2 = input.find(del);
  indx1 = 0;
  while(std::string::npos != indx2)
  {
    vec.push_back(input.substr(indx1, indx2-indx1));
    indx1 = indx2 + del.size();
    indx2 = input.find(del, indx1);
  }
  if(indx1 != input.length())
    vec.push_back(input.substr(indx1));
}



std::vector<Asset> readUni(char* file,int &num){
  std::vector<Asset> ass;
  std::ifstream ifs(file);
  if(!ifs.is_open()){
    perror("Failed to open the Universe file");
   	exit(EXIT_FAILURE);
  }
  std::string line;
  std::string del = ",";  

  while(std::getline(ifs,line)){
    std::vector<std::string> vec;
    split(line,vec,del);
    if(vec.size() != 3){
      perror("wrong number of data in one row");
   	  exit(EXIT_FAILURE);
    }

    Asset asset_t;
    asset_t.name = vec[0];
    if(atof(vec[1].c_str())==0.0 || atof(vec[2].c_str())==0.0){
      perror("Assets have non-numeric value");
   	  exit(EXIT_FAILURE);
    }
    asset_t.exr = atof(vec[1].c_str());
    asset_t.stdev = atof(vec[2].c_str());
    num++;
    ass.push_back(asset_t);
  }
  if(num == 0){
    perror("Empty file( universe )");
    exit(EXIT_FAILURE);
  }
  return ass;
}


MatrixXd readCorr(char* file,int num){
  std::ifstream ifs(file);
  if(!ifs.is_open()){
    perror("Failed to open the Correlation file");
   	exit(EXIT_FAILURE);
  }

  MatrixXd Corr(num,num);
  std::string line;
  std::string del = ","; 
  int x=0;
  while(std::getline(ifs,line)){
    if(x == num){
      perror("Dimension > #assets");
      exit(EXIT_FAILURE);
    }
    std::vector<std::string> vec;
    split(line,vec,del);
    if(vec.size() != (size_t)num){
      perror("Dimension != #assets");
      exit(EXIT_FAILURE);
    }
    for(int y=0;y<num;y++){
      if(atof(vec[y].c_str())==0.0){
        perror("Correlation matrix has non-numeric value");
   	    exit(EXIT_FAILURE);
      }
      Corr(x,y) = atof(vec[y].c_str());
    }
    x++;
  }

  
  if(x == 0){
    perror("Empty file (correlation.csv)");
    exit(EXIT_FAILURE);
  }
  for(int i=0;i<num;i++){
    for(int j=0;j<=i;j++){
      if(fabs(Corr(i,j) - Corr(j,i)) > 0.0001 || fabs(Corr(i,j)) > 1.0001){
        perror("Correlation matrix-incorrect format");
        exit(EXIT_FAILURE);
      }
    }
    if(fabs(Corr(i,i) - 1) > 0.0001){
      perror("Corr(i,i) != 1");
      exit(EXIT_FAILURE);
    }
  }

  return Corr;
}

double Unrestrict(Portfolio &port,double &r){
  int n = port.num;
  MatrixXd A1 = MatrixXd::Ones(1,n);
  MatrixXd A2(1,n);
  for(int i=0;i<n;i++){
    A2(i) = port.ass[i].exr;
  }
  MatrixXd A(2,n);
  A<<A1,A2;
  MatrixXd b1 = MatrixXd::Zero(n,1);
  MatrixXd b2(2,1);
  b2<<1,r;
  MatrixXd B(n+2,1);
  B<<b1,b2;
  MatrixXd Cov(n,n);
  for(int i=0;i<n;i++){
    for(int j=0;j<n;j++){
      Cov(i,j) = port.ass[i].stdev * port.Corr(i,j) * port.ass[j].stdev;
    }
  }
  MatrixXd C = MatrixXd::Zero(2,2);
  MatrixXd D(n+2,n+2);
  D<<Cov,A.transpose(),A,C;
  VectorXd V(n+2,1);
  V = D.fullPivHouseholderQr().solve(B);
  port.weight = V.head(n);
  port.calculateStd();
  return port.sigma;
}



double Restrict(Portfolio &port,double &r){
  int n = port.num;
  MatrixXd A1 = MatrixXd::Ones(1,n);
  MatrixXd A2(1,n);
  for(int i=0;i<n;i++){
    A2(i) = port.ass[i].exr;
  }
  MatrixXd A(2,n);
  A<<A1,A2;
  MatrixXd b1 = MatrixXd::Zero(n,1);
  MatrixXd b2(2,1);
  b2<<1,r;
  MatrixXd B(n+2,1);
  B<<b1,b2; 
  MatrixXd Cov(n,n);
  for(int i=0;i<n;i++){
    for(int j=0;j<n;j++){
      Cov(i,j) = port.ass[i].stdev * port.Corr(i,j) * port.ass[j].stdev;
    }
  }
  MatrixXd O = MatrixXd::Zero(2,2);
  MatrixXd K(n+2,n+2);
  K<<Cov,A.transpose(),A,O;
  VectorXd X(n+2,1);
  X = K.fullPivHouseholderQr().solve(B);
  MatrixXd A3 = A;
  MatrixXd B2 = B;
  VectorXd X2 = X;
  for(int i=1;;i++){
    int count = 1;
    MatrixXd R;
    int l=0;
    for(int j=0;j<n;j++){
      if(X2(j) < 0){
        MatrixXd M = MatrixXd::Zero(n,1);
        M(j,0) = 1;
        MatrixXd temp = R;
        R.resize(n,l+1);
        if(l==0){
          R<<M;  
        }
        else{
          R<<temp,M;
        }
        count = 0;
        l++;
      }
    }
    if(count == 1){
      break;
    }
    MatrixXd M;
    M = A3;
    A3.resize(M.rows() + R.cols(),n);
    A3<<M,R.transpose();
    M = B2;
    B2.resize(M.rows() + R.cols(),1);
    B2<<M,MatrixXd::Zero(R.cols(),1);
    MatrixXd OO = MatrixXd::Zero(A3.rows(),A3.rows());
    MatrixXd KK(Cov.rows() + A3.rows(),Cov.rows() + A3.rows());
    KK<<Cov,A3.transpose(),A3,OO;
    X2 = KK.fullPivHouseholderQr().solve(B2);
  }
  port.weight = X2.head(n);
  port.calculateStd();
  return port.sigma;
}
