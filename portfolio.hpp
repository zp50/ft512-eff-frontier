#ifndef __PORTFOLIO_HPP__
#define __PORTFOLIO_HPP__

#include "asset.hpp"
#include <cstdio>
#include <cstdlib>
#include <string>
#include <cmath>
#include <vector>
#include <Eigen/Dense>
using namespace Eigen;

class Portfolio {
  public:
    int num;
    double sigma;
    double ror;
    std::vector<Asset> ass;
    VectorXd weight;
    MatrixXd Corr;
  public:
    Portfolio(){}
    void calculateROR(){
      int sum = 0;
      for(int i=0;i<num;i++){
        sum += weight(i)*ass[i].exr;
      }
      ror = sum;
    }
    
    void calculateStd(){     
      double sum = 0;
      for(int i=0;i<num;i++){
        for(int j=0;j<num;j++){
          sum += weight(i)*weight(j)*Corr(i,j)*ass[i].stdev*ass[j].stdev;
        }
      }
      sigma = std::sqrt(sum);
    } 
    ~Portfolio(){}
};

#endif
