#include "parse.hpp"
#include "asset.hpp"
#include "portfolio.hpp"
#include <cstdio>
#include <cstdlib>
#include <string>
#include <vector>
#include <iomanip>
#include <unistd.h>
#include <Eigen/Dense>
#include <iostream>

using namespace Eigen;

int main(int argc, char ** argv){
  if (argc != 3 && argc !=4) {
    fprintf(stderr, "Input Error\n");
    return EXIT_FAILURE;
  }
  if(argc ==4 && strlen(argv[1]) != 2){
      fprintf(stderr, "-r has wrong size or pos\n");
      return EXIT_FAILURE;
  }

  int check;
  bool r = false;
  while ((check = getopt(argc, argv, "r")) != -1){
    switch(check){
      case 'r':
        r = true;
        break;
      case '?':
        fprintf(stderr, "-r error\n");
        return EXIT_FAILURE;
    }
  }


  int num = 0;
  std::vector<Asset> ass;
  MatrixXd C;
  if(r == true){
    ass = readUni(argv[2],num);
    C = readCorr(argv[3],num);
  }
  else{
    ass = readUni(argv[1],num);
    C = readCorr(argv[2],num); 
  }

  Portfolio port;
  
  port.Corr = C;
  port.ass = ass;
  port.num = num;


  std::cout<<"ROR,volatility"<<std::endl;
  std::cout.setf(std::ios::fixed);
  if(r == false){
    for(double i = 0.01;i <= 0.265;i += 0.01){
      std::cout<<std::fixed<<std::setprecision(1)<<i*100<<"%,";
      std::cout<<std::fixed<<std::setprecision(2)<<Unrestrict(port,i)*100<<"%"<<std::endl;
    }
  }
  if(r == true){
    for(double i = 0.01;i <= 0.265;i += 0.01){
      std::cout<<std::fixed<<std::setprecision(1)<<i*100<<"%,";
      std::cout<<std::fixed<<std::setprecision(2)<<Restrict(port,i)*100<<"%"<<std::endl;
    }
  }
  std::cout.unsetf(std::ios::fixed);
  return EXIT_SUCCESS;
}
